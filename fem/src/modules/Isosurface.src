!/*****************************************************************************/
! *
! *  Elmer, A Finite Element Software for Multiphysical Problems
! *
! *  Copyright 1st April 1995 - , CSC - IT Center for Science Ltd., Finland
! * 
! *  This program is free software; you can redistribute it and/or
! *  modify it under the terms of the GNU General Public License
! *  as published by the Free Software Foundation; either version 2
! *  of the License, or (at your option) any later version.
! * 
! *  This program is distributed in the hope that it will be useful,
! *  but WITHOUT ANY WARRANTY; without even the implied warranty of
! *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! *  GNU General Public License for more details.
! *
! *  You should have received a copy of the GNU General Public License
! *  along with this program (in file fem/GPL-2); if not, write to the 
! *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, 
! *  Boston, MA 02110-1301, USA.
! *
! *****************************************************************************/
!
! ******************************************************************************
! *
! *  Authors: Juha Ruokolainen
! *  Email:   jpr@csc.fi
! *  Web:     http://www.csc.fi/elmer
! *  Address: CSC - IT Center for Science Ltd.
! *           Keilaranta 14
! *           02101 Espoo, Finland 
! *
! *  Original Date: 20.06.2007
! *
! *****************************************************************************/



!------------------------------------------------------------------------------
!> Subroutine for extracting isosurfaces in 3d and isolines in 2d.
!> The result will be a new mesh which will be added to the list of meshes.
!> \ingroup Solvers
!------------------------------------------------------------------------------
SUBROUTINE IsosurfaceSolver( Model,Solver,dt,Transient )
!------------------------------------------------------------------------------

  USE CoordinateSystems
  USE DefUtils

  IMPLICIT NONE
!------------------------------------------------------------------------------
  TYPE(Solver_t) :: Solver  !< Linear & nonlinear equation solver options
  TYPE(Model_t) :: Model    !< All model information (mesh, materials, BCs, etc...)
  REAL(KIND=dp) :: dt       !< Timestep size for time dependent simulations
  LOGICAL :: Transient      !< Steady state or transient simulation
!------------------------------------------------------------------------------
  CHARACTER(LEN=MAX_NAME_LEN) :: LevelVariableName
  TYPE(Mesh_t), POINTER :: Mesh, IsoMesh => Null(), Pmesh, OrigMesh
  INTEGER :: i,j,k,l,n, dim,NoLevels,Level,NoNewElements
  REAL(KIND=dp) :: LevelValue, LevelDiff

  INTEGER, TARGET :: TetraToTetraMap(1,4), PyramidToTetraMap(2,4), &
             WedgeToTetraMap(3,4), BrickToTetraMap(5,4), &
             TriangleToTriangleMap(1,3), QuadToTriangleMap(2,3)
  INTEGER, POINTER :: Map(:,:), Indexes(:)
  INTEGER :: NoOrigElements,calls=0,ierr

  TYPE(Variable_t), POINTER :: LevelVariable
  LOGICAL :: CreateDummyMesh, MovingMesh, Found
  LOGICAL, ALLOCATABLE :: ElemSplit(:)
  INTEGER, ALLOCATABLE :: eperm(:), InvPerm(:), ElemPerm(:)
  INTEGER :: NewElemType, NewElemNodes, LevelDofs, NoEdges, IsAllocated, &
      NoIsoNodes, NoSurfaces
  INTEGER :: ParSizes(6), ParTmp(6)
  REAL(KIND=dp), ALLOCATABLE :: x(:),y(:),z(:),ElemFun(:)
  REAL(KIND=dp), POINTER :: LevelFun(:), LevelValues(:,:), Interpolant(:)
  INTEGER, POINTER :: LevelPerm(:)
  TYPE(Element_t), POINTER  :: OrigElements(:), NewElements(:), Element
  TYPE(ValueList_t), POINTER :: Params
  LOGICAL :: Visited = .FALSE., FixedSurface, SurfaceExist = .FALSE.

  SAVE Visited, FixedSurface, SurfaceExist, Interpolant, InvPerm, Isomesh, &
	NewElements, NoNewElements


  CALL Info( 'IsosurfaceSolver','-------------------------------------',Level=4 )
  CALL Info( 'IsosurfaceSolver','Determining the isosurface',Level=4 )
  CALL Info( 'IsosurfaceSolver','-------------------------------------',Level=4 )

  Mesh => GetMesh()
  OrigMesh => Mesh
  NoOrigElements = Mesh % NumberOfBulkElements
  OrigElements => Mesh % Elements
  NoNewElements = 0
  NewElements => NULL()
  dim = Mesh % MeshDim
  ! this is just a temporary hack to live with old library
  IF(dim < 1 .OR. dim > 3 ) dim = 3

  Params => GetSolverParams()
  FixedSurface = GetLogical( Params,'Isosurface Fixed',Found)
  MovingMesh = GetLogical( Params,'Moving Mesh',Found)

  !---------------------------------------------------------------
  ! Get the isosurface variable
  !---------------------------------------------------------------
  LevelVariableName = GetString( Params,'Isosurface Variable')
  LevelVariable => VariableGet( Mesh % Variables, LevelVariableName )

  IF (.NOT.ASSOCIATED(LevelVariable)) THEN
    CALL Error( 'Isosurface', 'Missing isosurface variable: ' // &
        TRIM(LevelVariableName) )
    RETURN
  END IF

  Levelfun => LevelVariable % Values
  LevelPerm => LevelVariable % Perm
  LevelDofs = LevelVariable % Dofs
  
  !---------------------------------------------------------------
  ! Check the isosurface values
  !---------------------------------------------------------------
  NoLevels = 0
  LevelValues => ListGetConstRealArray( Params,'Isosurface values',Found)
  IF( Found ) THEN
    NoLevels = SIZE(LevelValues,1)
    LevelValue = LevelValues(1,1)
  ELSE
    LevelValue = ListGetCReal( Params,'Isosurface value',Found)
    IF( Found ) NoLevels = 1
  END IF

  IF(.NOT. Found ) THEN
    CALL Warn('IsosurfaceSolver','Could not determine Isosurface value')
    RETURN
  END IF

  !--------------------------------------------------------------------------
  ! If the mesh was created and will be fixed make a simplified interpolation
  !--------------------------------------------------------------------------
  IF( FixedSurface .AND. SurfaceExist ) THEN
    CALL Info('IsosurfaceSolver','Remapping the fields')

    Mesh % NumberOfBulkElements = NoNewElements
    Mesh % Elements => NewElements

    CALL ReMap()

    Mesh % NumberOfBulkElements = NoOrigElements
    Mesh % Elements => OrigElements

    RETURN
  END IF

  n = Mesh % MaxElementNodes
  ALLOCATE(ElemFun(n), ElemPerm(n))

  !---------------------------------------------------------------
  ! Map any element to tetrahedron or triangle
  !---------------------------------------------------------------
  TetraToTetraMap(1,:) = [1,2,3,4]

  PyramidToTetraMap(1,:) = [3,5,4,1]
  PyramidToTetraMap(2,:) = [3,5,2,1]

  WedgeToTetraMap(1,:) = [5,4,3,1]
  WedgeToTetraMap(2,:) = [5,3,2,1]
  WedgeToTetraMap(3,:) = [5,6,4,3]

  BrickToTetraMap(1,:) = [1,2,4,5]
  BrickToTetraMap(2,:) = [6,7,2,5]
  BrickToTetraMap(3,:) = [3,4,2,7]
  BrickToTetraMap(4,:) = [8,7,5,4]
  BrickToTetraMap(5,:) = [2,4,5,7]

  TriangleToTriangleMap(1,:) = [1,2,3]
  QuadToTriangleMap(1,:) = [1,2,3]
  QuadToTriangleMap(2,:) = [1,3,4]

  !---------------------------------------------------------------
  ! Check whether mesh is all-tets or all-triangles
  !---------------------------------------------------------------
  CreateDummyMesh = .FALSE.
  DO i=1,Mesh % NumberOfBulkElements
    Element => Mesh % Elements( i ) 
    j = GetElementFamily( Element )
    IF( dim == 2 .AND. j /= 3 ) THEN
      CreateDummyMesh = .TRUE.
      EXIT
    END IF
    IF( dim == 3 .AND. j /= 5 ) THEN
      CreateDummyMesh = .TRUE.
      EXIT
    END IF
  END DO

  IF( dim == 2 ) THEN
    NewElemType = 303
    NewElemNodes = 3
  ELSE
    NewElemType = 504
    NewElemNodes = 4
  END IF

  !---------------------------------------------------------------
  ! Create a temporary mesh consisting only of tets (or triangles)
  ! in where the isosurface (or isoline) will be determined.
  ! The temporal mesh includes only potential master elements.
  !---------------------------------------------------------------
  IF ( CreateDummyMesh ) THEN
    CALL Info('IsoSurfaceSolver','Creating a temporal mesh')
    IF( NoLevels > 1 ) ALLOCATE( ElemSplit( Mesh % NumberOfBulkElements ) )
    
    DO IsAllocated = 0, 1
      
      j = 0
      IF( NoLevels > 1 ) ElemSplit = .FALSE.
      
      DO Level = 1, NoLevels
        IF( NoLevels > 1 ) LevelValue = LevelValues(Level,1) 
        
        DO i=1,Mesh % NumberOfBulkElements
          
          Element => Mesh % Elements(i)
          Indexes => Element % NodeIndexes
          n = Element % TYPE % NumberOfNodes
          
          IF( NoLevels > 1 ) THEN
            IF( ElemSplit(i) ) CYCLE
          END IF
          
          IF( ASSOCIATED( LevelPerm ) ) THEN
            ElemPerm(1:n) = LevelPerm( Indexes ) 
            IF( ANY ( LevelPerm( Indexes) == 0 ) ) CYCLE
          ELSE 
            ElemPerm(1:n) = Indexes(1:n)
          END IF
          
          SELECT CASE(GetElementFamily( Element ))
          CASE(3); Map => TriangleToTriangleMap
          CASE(4); Map => QuadToTriangleMap
          CASE(5); Map => TetraToTetraMap
          CASE(6); Map => PyramidToTetraMap
          CASE(7); Map => WedgeToTetraMap
          CASE(8); Map => BrickToTetraMap
          END SELECT
          
          IF( LevelDofs == 1 ) THEN
            ElemFun(1:n) =  LevelFun( ElemPerm(1:n) )
          ELSE
            ElemFun(1:n) = 0.0_dp
            DO l=1,MIN(dim,LevelDofs)
              ElemFun(1:n) = ElemFun(1:n) + LevelFun( LevelDofs * ( ElemPerm(1:n) - 1) + l )**2
            END DO
            ElemFun(1:n) = SQRT( ElemFun(1:n) )
          END IF
          ElemFun(1:n) = ElemFun(1:n) - LevelValue
          IF( ALL ( ElemFun(1:n) < 0.0_dp ) ) CYCLE
          IF( ALL ( ElemFun(1:n) > 0.0_dp ) ) CYCLE
          
          IF( NoLevels > 1 ) THEN
            IF( ElemSplit(i) ) CYCLE
          END IF
          
          IF( IsAllocated == 0 ) THEN
            j = j + SIZE(Map,1)
            CYCLE
          END IF
          
          DO k=1,SIZE(Map,1)
            j = j + 1
            NewElements(j) = Element
            NewElements(j) % TYPE => GetElementType(NewElemType)
            ALLOCATE(NewElements(j) % NodeIndexes(NewElemNodes))
            DO l=1,NewElemNodes
              NewElements(j) % NodeIndexes(l) = &
                  Element % NodeIndexes(Map(k,l))
            END DO
          END DO
        END DO
      END DO
      
      NoNewElements = j	
      
      IF( NoNewElements == 0 ) EXIT
      
      IF( IsAllocated == 0 ) THEN
        ALLOCATE(NewElements(NoNewElements))
      END IF
    END DO
    
    IF( NoLevels > 1 ) DEALLOCATE( ElemSplit ) 
    
    IF( NoNewElements > 0 ) THEN
      Mesh % NumberOfBulkElements = NoNewElements
      Mesh % Elements => NewElements
    END IF
  END IF

  ! Find mesh edges in order to define the intersection points
  !-----------------------------------------------------------
  NoEdges = 0
  IF( .NOT. CreateDummyMesh .OR. NoNewElements > 0 ) THEN
    IF (.NOT.ASSOCIATED(Mesh % Edges)) THEN
      IF( dim == 2 ) THEN
        CALL FindMeshEdges2D(Mesh)
      ELSE
        CALL FindMeshEdges3D(Mesh)
      END IF
    END IF
    NoEdges = Mesh % NumberOfEdges
  END IF

  ! If the mesh was previously created release it before recreating
  !----------------------------------------------------------------
  IF( SurfaceExist  ) THEN
    Pmesh => Model % Meshes
    DO WHILE(ASSOCIATED(Pmesh % Next))
      IF ( ASSOCIATED(Pmesh % next, Isomesh)) THEN
        Pmesh % next => Isomesh % Next
        EXIT
      END IF
      Pmesh => Pmesh % next
    END DO
    CALL ReleaseMesh(Isomesh)
    SurfaceExist = .FALSE.
  END IF
  
  ! Create a new mesh for isosurface
  !----------------------------------------------------------------
  Isomesh => AllocateMesh()
  IsoMesh % Name = GetString( Params,'Mesh Name', Found )
  IF (.NOT. Found ) THEN
    IF( dim == 2 ) THEN
      IsoMesh % Name = "isoline"
    ELSE
      IsoMesh % Name = "isosurf"
    END IF
  END IF
  Isomesh % Changed = .TRUE.
  Isomesh % OutputActive  = .TRUE.
  Isomesh % NumberOfBulkElements = 0
  Isomesh % NumberOfNodes = 0
  SurfaceExist = .TRUE.
  
  ! If requested number the output directories
  ! Alternatively one can number the output files
  !----------------------------------------------------------------
  IF( GetLogical(Params,'Mesh Numbering',Found) ) THEN
    Calls = Calls + 1
    Isomesh % name = TRIM(Isomesh % name) // TRIM(i2s(calls))
  END IF
  CALL MakeDirectory( TRIM(Isomesh % name) // CHAR(0) )
  
  ! Create nodes and elements on the edge intersections  
  !----------------------------------------------------------------
  NoIsoNodes = CreateNodes()    
  NoSurfaces = CreateSurfaces()
    
  ! Release temporary structures and revert original mesh
  !----------------------------------------------------------------------	
  IF(.NOT. FixedSurface ) THEN
    IF( NoEdges > 0 ) CALL ReleaseMeshEdgeTables( Mesh )
    IF ( CreateDummyMesh .AND. ASSOCIATED( NewElements ) ) DEALLOCATE( NewElements )
  END IF

  IF( ALLOCATED( ElemFun ) ) DEALLOCATE( ElemFun, ElemPerm )  
  Mesh % NumberOfBulkElements = NoOrigElements
  Mesh % Elements => OrigElements

  ! Add the new mesh into the list 
  !----------------------------------------------------------------------
  PMesh => Model % Meshes
  DO WHILE( ASSOCIATED(PMesh % Next) )
    PMesh => PMesh % Next
  END DO
  PMesh % Next => Isomesh 

  ! Information of the new system size, also in parallel
  !----------------------------------------------------------------------
  ParTmp(1) = Mesh % NumberOfNodes
  ParTmp(2) = NoOrigElements 
  ParTmp(3) = NoNewElements 
  ParTmp(4) = NoEdges
  ParTmp(5) = NoIsoNodes
  ParTmp(6) = NoSurfaces

  IF( ParEnv % PEs > 1 ) THEN
    CALL MPI_ALLREDUCE(ParTmp,ParSizes,6,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,ierr)
  ELSE
    ParSizes = ParTmp
  END IF

  CALL Info('IsoSurfaceSolver','Information on isosurface mesh sizes')

  WRITE ( Message,'(A,I0,A)') 'Initial mesh has ',ParSizes(1),' nodes'
  CALL Info('IsoSurfaceSolver',Message)
  WRITE ( Message,'(A,I0,A)') 'Initial mesh has ',ParSizes(2),' elements'
  CALL Info('IsoSurfaceSolver',Message)
  IF( ParSizes(3) > 0 ) THEN
    WRITE ( Message,'(A,I0,A)') 'Temporal mesh has ',ParSizes(3),' elements'
    CALL Info('IsoSurfaceSolver',Message)
  END IF
  WRITE ( Message,'(A,I0,A)') 'Working mesh has ',ParSizes(4),' edges'
  CALL Info('IsoSurfaceSolver',Message)
  WRITE ( Message,'(A,I0,A)') 'Isosurface mesh has ',ParSizes(5),' nodes'
  CALL Info('IsoSurfaceSolver',Message)
  WRITE ( Message,'(A,I0,A)') 'Isosurface mesh has ',ParSizes(6),' elements'
  CALL Info('IsoSurfaceSolver',Message)

CONTAINS

  !----------------------------------------------------------------
  !> Create nodes for the isosurface / isoline.
  !----------------------------------------------------------------
  FUNCTION CreateNodes() RESULT ( NoIsoNodes )

    INTEGER :: NoIsoNodes
    TYPE(Element_t), POINTER :: Edge
    INTEGER :: i,j,k,l,n1,n2,m1,m2,ints,comps
    REAL(KIND=dp) :: t,t1,t2,x1,x2,y1,y2,z1,z2

    CHARACTER(LEN=MAX_NAME_LEN) :: Vname(32)
    LOGICAL :: Found
    TYPE(Variable_t), POINTER :: Vfull, Viso
    INTEGER, POINTER :: Vperm(:)
    REAL(KIND=dp), POINTER :: Vals(:)


    ! Make a table of the field variables and allocate space
    !----------------------------------------------------------------
    NoIsoNodes = 0
    ints = 0
    DO WHILE(.TRUE.)
      ints = ints+1
      Vname(ints) = GetString( Params, &
          ComponentName('Isosurface interpolant',ints), Found )
      IF(.NOT. Found) Vname(ints) = GetString( Params, &
          ComponentName('Variable',ints), Found )
      IF ( .NOT. Found ) THEN
        ints = ints-1
        EXIT
      END IF
      Vfull => VariableGet( Mesh % Variables, Vname(ints) )
      IF(.NOT. ASSOCIATED( Vfull ) ) THEN
        ints = ints - 1
      END IF
    END DO

    ! For parallel runs create the variables even though they do not exist since they 
    ! will do so in other partitions.
    !--------------------------------------------------------------------------------
    NoEdges = Mesh % NumberOfEdges
    IF( NoEdges == 0 ) THEN
      IF ( ints > 0 .AND. ParEnv % PEs > 1 ) THEN
        DO i=1,ints
          Vfull => VariableGet( Mesh % Variables, Vname(i) )
          NULLIFY( Vperm, Vals )
          ALLOCATE( Vperm(1),Vals(Vfull % DOFs) )
          Vperm = 0
          Vals = 0.0_dp
          CALL Info('IsoSurfaceSolver','Creating dummy variable '//TRIM( Vname(i) ) )
          CALL VariableAddVector( Isomesh % Variables, Isomesh, Solver, &
               TRIM(Vname(i)), Vfull % DOFs, Vals, Vperm )
        END DO
      END IF
      RETURN
    END IF


    ! Loop over edges and check for different signs
    !----------------------------------------------------------------
    DO IsAllocated=0,1

      j = 0            
      DO Level = 1, NoLevels
        IF( NoLevels > 1 ) LevelValue = LevelValues(Level,1) 
        
        DO i=1,NoEdges
          Edge => Mesh % Edges(i)
          
          n1 = Edge % NodeIndexes(1)
          n2 = Edge % NodeIndexes(2)
          
          IF( ASSOCIATED( LevelPerm ) ) THEN
            m1 = LevelPerm( n1 )
            m2 = LevelPerm( n2 )
          ELSE
            m1 = n1
            m2 = n2
          END IF
          
          IF ( m1 <= 0 .OR. m2 <= 0 ) CYCLE
          
          IF( LevelDofs == 1 ) THEN
            t1 = LevelFun( m1 ) 
            t2 = LevelFun( m2 ) 
          ELSE
            t1 = 0.0_dp
            t2 = 0.0_dp
            DO k=1,MIN(dim,LevelDofs)
              t1 = t1 + LevelFun( LevelDofs*(m1-1) + k )**2
              t2 = t2 + LevelFun( LevelDofs*(m2-1) + k )**2
            END DO
            t1 = SQRT( t1 )
            t2 = SQRT( t2 )
          END IF
          
          t1 = t1 - LevelValue
          t2 = t2 - LevelValue
          
          IF( ABS( t1 - t2 ) < TINY( t1 ) ) CYCLE
          IF ( t1 * t2 > 0.0_dp ) CYCLE
          
          j = j + 1
          IF( IsAllocated == 0 ) CYCLE

          !---------------------------------------------------------
          ! Only in the second loop tabulate the values
          !---------------------------------------------------------

          t = ABS( t1/(t2-t1) )

          IF( FixedSurface ) THEN
            Interpolant(j) = t
            InvPerm(j) = i
          END IF
          
          x1 = Mesh % Nodes % x(n1)
          x2 = Mesh % Nodes % x(n2)
          
          y1 = Mesh % Nodes % y(n1)
          y2 = Mesh % Nodes % y(n2)
          
          z1 = Mesh % Nodes % z(n1)
          z2 = Mesh % Nodes % z(n2)
          
          eperm(i) = j
          Isomesh % Nodes % x(j) = (1-t) * x1 + t * x2
          Isomesh % Nodes % y(j) = (1-t) * y1 + t * y2
          Isomesh % Nodes % z(j) = (1-t) * z1 + t * z2
          
          DO k=1,ints
            Vfull => VariableGet(Mesh % Variables, Vname(k))
            Viso => VariableGet(IsoMesh % Variables, Vname(k))

            IF( ASSOCIATED( Vfull % Perm ) ) THEN
              m1 = Vfull % Perm( n1 )
              m2 = Vfull % Perm( n2 )
            ELSE
              m1 = n1
              m2 = n2
            END IF
            IF( m1 <= 0 .OR. m2 <= 0 ) CYCLE
            
            DO l=1,Vfull % DOFs
              x1 = Vfull % Values(Vfull % DOFs*(m1-1)+l)
              x2 = Vfull % Values(Vfull % DOFs*(m2-1)+l)
              Viso % Values(Vfull % DOFs*(j-1)+l) = (1-t)*x1 + t*x2
            END DO
          END DO
        END DO
      END DO
      

      IF( IsAllocated == 0 ) THEN
        NoIsoNodes = j
        ALLOCATE( IsoMesh % Nodes )
        Isomesh % NumberOfNodes = j
        Isomesh % Nodes % NumberOfNodes = j
        Isomesh % MeshDim = dim
       
        ALLOCATE( IsoMesh % Nodes % x(j) )
        ALLOCATE( IsoMesh % Nodes % y(j) )
        ALLOCATE( IsoMesh % Nodes % z(j) )

        ! Gives the index of the node sitting on a edge
        ALLOCATE( Eperm(NoEdges) ) 
        Eperm = 0
        
        CALL VariableAdd( IsoMesh % Variables, IsoMesh,Solver, &
            'Coordinate 1',1,IsoMesh % Nodes % x )
        
        CALL VariableAdd( IsoMesh % Variables,IsoMesh,Solver, &
            'Coordinate 2',1,IsoMesh % Nodes % y )
        
        CALL VariableAdd( IsoMesh % Variables,IsoMesh,Solver, &
            'Coordinate 3',1,IsoMesh % Nodes % z )
        
        Vfull => VariableGet( Mesh % Variables, 'Time' )
        CALL VariableAdd( Isomesh % Variables, Isomesh, Solver, 'Time', 1, &
            Vfull % Values )
        
        DO k=1,ints
          Vfull => VariableGet( Mesh % Variables, Vname(k) )
          NULLIFY( Vperm, Vals )
          ALLOCATE( Vperm(j),Vals(Vfull % DOFs*j) )
          Vperm = [(i,i=1,j)]
          Vals = 0.0_dp
          
          CALL Info('IsoSurfaceSolver','Creating variable '//TRIM( Vname(k) ) )
          CALL VariableAddVector( Isomesh % Variables, Isomesh, Solver, &
              TRIM(Vname(k)), Vfull % DOFs, Vals, Vperm )
        END DO
	j = 0

        IF( FixedSurface ) THEN
          ALLOCATE( InvPerm( NoIsonodes ), Interpolant( NoIsoNodes) )
          InvPerm = 0
          Interpolant = 0.0_dp
        END IF

      END IF

    END DO

  END FUNCTION CreateNodes


  !----------------------------------------------------------------
  !> Remap the field values to the isosurface.
  !----------------------------------------------------------------
  SUBROUTINE ReMap()

    TYPE(Element_t), POINTER :: Edge
    INTEGER :: i,j,k,l,n1,n2,m1,m2,ints
    REAL(KIND=dp) :: t,x1,x2

    CHARACTER(LEN=MAX_NAME_LEN) :: Vname(32)
    LOGICAL :: Found
    TYPE(Variable_t), POINTER :: Vfull, Viso


    ! If there are no edges then the dummy field has already been created
    !--------------------------------------------------------------------------------
    IF( NoEdges == 0 ) RETURN

    ! Make a table of the field variables and allocate space
    !----------------------------------------------------------------
    n = NoEdges
    ints = 0
    DO WHILE(.TRUE.)
      ints = ints+1
      Vname(ints) = GetString( Params, &
          ComponentName('Isosurface interpolant',ints), Found )
      IF(.NOT. Found) Vname(ints) = GetString( Params, &
          ComponentName('Variable',ints), Found )
      IF ( .NOT. Found ) THEN
        ints = ints-1
        EXIT
      END IF
      Vfull => VariableGet( Mesh % Variables, Vname(ints) )
      IF(.NOT. ASSOCIATED( Vfull ) ) THEN
        ints = ints - 1
      END IF
    END DO

    ! Loop over predifined nodes
    !----------------------------------------------------------------
    DO Level = 1, NoLevels
      IF( NoLevels > 1 ) LevelValue = LevelValues(Level,1) 

      DO j=1,Isomesh % NumberOfNodes
        i = InvPerm(j)

        Edge => Mesh % Edges(i)
        
        n1 = Edge % NodeIndexes(1)
        n2 = Edge % NodeIndexes(2)
        
        t = Interpolant(j)

        ! If the mesh is moving then remap the also the coordinate values
        !----------------------------------------------------------------
        IF( MovingMesh ) THEN
          Isomesh % Nodes % x(j) = &
              (1-t) * Mesh % Nodes % x(n1) + t * Mesh % Nodes % x(n2)
          Isomesh % Nodes % y(j) = &
              (1-t) * Mesh % Nodes % y(n1) + t * Mesh % Nodes % y(n2)
          Isomesh % Nodes % z(j) = &
              (1-t) * Mesh % Nodes % z(n1) + t * Mesh % Nodes % z(n2)
        END IF

        
        DO k=1,ints
          Vfull => VariableGet(Mesh % Variables, Vname(k))
          IF (.NOT. ASSOCIATED(Vfull)) CYCLE

          Viso => VariableGet(IsoMesh % Variables, Vname(k))
          IF (.NOT. ASSOCIATED(Viso)) CYCLE

          IF( ASSOCIATED( Vfull % Perm )) THEN                
            m1 = Vfull % Perm(n1)
            m2 = Vfull % Perm(n2)
          ELSE
            m1 = n1
            m2 = n2 
          END IF
          IF( m1 <= 0 .OR. m2 <= 0 ) CYCLE

          DO l=1,Vfull % DOFs
            x1 = Vfull % Values(Vfull % DOFs*(m1-1)+l)
            x2 = Vfull % Values(Vfull % DOFs*(m2-1)+l)
            Viso % Values(Viso % DOFs*(j-1)+l) = (1-t)*x1 + t*x2
          END DO
        END DO
      END DO
    END DO

  END SUBROUTINE ReMap

  !----------------------------------------------------------------
  !> Create the isosurfaces or isolines.
  !----------------------------------------------------------------
  FUNCTION CreateSurfaces() RESULT ( NoSurfaces )
     INTEGER :: NoSurfaces
     TYPE(Element_t), POINTER :: Element, DefElement
     REAL(KIND=dp) :: F(4)
     INTEGER :: i,j,k,n,Triangles(2,3),Line(2)
     INTEGER :: NewElemType, NewElemNodes

     NoSurfaces = 0
     IF( NoIsoNodes == 0 ) RETURN

     IF( dim == 3 ) THEN
       NewElemType = 303
       NewElemNodes = 3
     ELSE
       NewElemType = 202
       NewElemNodes = 2
     END IF

     DO IsAllocated = 0, 1
       k = 0

       DO Level = 1, NoLevels
         IF( NoLevels > 1 ) LevelValue = LevelValues(Level,1) 

         DO i=1,Mesh % NumberOfBulkElements
           Element => Mesh % Elements(i)
           n = Element % TYPE % NumberOfNodes
           
           IF( ASSOCIATED( LevelPerm ) ) THEN
             ElemPerm(1:n) = LevelPerm(Element % NodeIndexes)
           ELSE
             ElemPerm(1:n) = Element % NodeIndexes
           END IF
           
           IF( LevelDofs == 1 ) THEN
             F(1:n) = LevelFun( ElemPerm(1:n) )
           ELSE
             F(1:n) = 0.0_dp
             DO j=1,MIN(dim,LevelDofs)
               F(1:n) = F(1:n) + LevelFun( LevelDofs*(ElemPerm(1:n)-1)+j)**2
             END DO
             F(1:n) = SQRT( F(1:n) ) 
           END IF
           F(1:n) = F(1:n) - LevelValue
                      
           IF( dim == 2 ) THEN
             IF( CreateLineFromTriangle(Element,F,Line) ) THEN
               k = k + 1
               IF( IsAllocated == 1) THEN
                 Isomesh % Elements(k) % NodeIndexes = Line
                 Isomesh % Elements(k) % BodyId = Level
               END IF
             END IF
           ELSE
             n = CreateSurfaceFromTetra(Element,F,Triangles)
             DO j=1,n
               IF ( ALL (Triangles(j,:) > 0 ) ) THEN
                 k = k + 1
                 IF( IsAllocated == 1) THEN
                   Isomesh % Elements(k) % NodeIndexes = Triangles(j,:)
                   Isomesh % Elements(k) % BodyId = Level
                 END IF
               END IF
             END DO
             
           END IF
         END DO
       END DO

       IF( k == 0 ) RETURN

       IF( IsAllocated == 0 ) THEN

	 NoSurfaces = k

         ALLOCATE( Isomesh % Elements(k) )
         
         Isomesh % MeshDim = dim 
         Isomesh % NumberOfBulkElements = k
         Isomesh % NumberOfFaces = 0
         Isomesh % NumberOfEdges = 0
         Isomesh % NumberOfBoundaryElements = 0
        
         DefElement => AllocateElement()
         DefElement % TYPE => GetElementType(NewElemType)
         DO i=1,k
           Isomesh % Elements(i) = DefElement
!           Isomesh % Elements(i) % TYPE => GetElementType(NewElemType)
           ALLOCATE(Isomesh % Elements(i) % NodeIndexes(NewElemNodes))
         END DO
       END IF
     END DO

     DEALLOCATE(DefElement)      
     
  END FUNCTION CreateSurfaces


  !----------------------------------------------------------------
  !> Create isosurfaces related to one tetrahedral element.
  !----------------------------------------------------------------
  FUNCTION CreateSurfaceFromTetra(Tetra,F,Surf) RESULT(scount)
    TYPE(Element_t) :: Tetra
    REAL(KIND=dp) :: F(4)
    INTEGER :: scount, Surf(2,3)

    LOGICAL :: S1,S2,S3,S4
    INTEGER :: S,H,i,j,l,Indexes(6)

    scount = 0
    Indexes = eperm(Tetra % EdgeIndexes)

    S1 = F(1) > 0.0_dp;
    S2 = F(2) > 0.0_dp;
    S3 = F(3) > 0.0_dp;
    S4 = F(4) > 0.0_dp;

    S = 0
    IF ( S1 ) S = S + 1
    IF ( S2 ) S = S + 1
    IF ( S3 ) S = S + 1
    IF ( S4 ) S = S + 1

    IF ( S==0 .OR. S==4 ) RETURN

    IF ( S==1 .OR. S==3 ) THEN
      scount = 1
      IF ( (S==1 .AND. S1) .OR. (S==3 .AND. .NOT.S1) ) THEN
        Surf(1,1) = Indexes(1)
        Surf(1,2) = Indexes(3)
        Surf(1,3) = Indexes(4)
      ELSE IF ( (S==1 .AND. S2) .OR. (S==3 .AND. .NOT.S2) ) THEN
        Surf(1,1) = Indexes(1)
        Surf(1,2) = Indexes(2)
        Surf(1,3) = Indexes(5)
      ELSE IF ( (S==1 .AND. S3) .OR. (S==3 .AND. .NOT.S3) ) THEN
        Surf(1,1) = Indexes(2)
        Surf(1,2) = Indexes(3)
        Surf(1,3) = Indexes(6)
      ELSE IF ( (S==1 .AND. S4) .OR. (S==3 .AND. .NOT.S4) ) THEN
        Surf(1,1) = Indexes(4)
        Surf(1,2) = Indexes(5)
        Surf(1,3) = Indexes(6)
      ELSE
        scount=0
      END IF
    ELSE
      scount = 2
      IF ( (S1 .AND. S2) .OR. (.NOT.S1 .AND. .NOT.S2) ) THEN
        Surf(1,1) = Indexes(3)
        Surf(1,2) = Indexes(2)
        Surf(1,3) = Indexes(5)

        Surf(2,1) = Indexes(3)
        Surf(2,2) = Indexes(5)
        Surf(2,3) = Indexes(4)
      ELSE IF ( (S1 .AND. S3) .OR. (.NOT.S1 .AND. .NOT.S3) ) THEN
        Surf(1,1) = Indexes(1)
        Surf(1,2) = Indexes(2)
        Surf(1,3) = Indexes(6)

        Surf(2,1) = Indexes(1)
        Surf(2,2) = Indexes(6)
        Surf(2,3) = Indexes(4)
      ELSE IF ( (S1 .AND. S4) .OR. (.NOT.S1 .AND. .NOT.S4) ) THEN
        Surf(1,1) = Indexes(1)
        Surf(1,2) = Indexes(5)
        Surf(1,3) = Indexes(6)

        Surf(2,1) = Indexes(1)
        Surf(2,2) = Indexes(6)
        Surf(2,3) = Indexes(3)
      ELSE
        scount=0
      END IF
    END IF

  END FUNCTION CreateSurfaceFromTetra

  
  !----------------------------------------------------------------
  !> Create isoline related to one triangular element.
  !----------------------------------------------------------------
  FUNCTION CreateLineFromTriangle(Triangle,F,Line) RESULT(GotLine)
    TYPE(Element_t) :: Triangle
    REAL(KIND=dp) :: F(3)
    INTEGER :: Line(2)
    LOGICAL :: GotLine

    LOGICAL :: S1,S2,S3
    INTEGER :: S,H,i,j,l,Indexes(3)

    GotLine = .FALSE.
    Indexes = eperm(Triangle % EdgeIndexes)

    S1 = F(1) > 0.0_dp;
    S2 = F(2) > 0.0_dp;
    S3 = F(3) > 0.0_dp;

    S = 0
    IF ( S1 ) S = S + 1
    IF ( S2 ) S = S + 1
    IF ( S3 ) S = S + 1

    IF ( S==0 .OR. S==3 ) RETURN

    GotLine = .TRUE.

    IF ( S == 1 ) THEN
      IF ( S1 ) THEN
        Line(1) = Indexes(1)
        Line(2) = Indexes(3)
      ELSE IF( S2 ) THEN
        Line(1) = Indexes(1)
        Line(2) = Indexes(2)
      ELSE
        Line(1) = Indexes(2)
        Line(2) = Indexes(3)
      END IF
    ELSE
      IF ( .NOT. S1 ) THEN
        Line(1)=Indexes(1)
        Line(2)=Indexes(3)
      ELSE IF( .NOT. S2 ) THEN
        Line(1) = Indexes(1)
        Line(2) = Indexes(2)
      ELSE
        Line(1) = Indexes(2)
        Line(2) = Indexes(3)
      END IF
    END IF

  END FUNCTION CreateLineFromTriangle

!------------------------------------------------------------------------------
END SUBROUTINE IsosurfaceSolver
!------------------------------------------------------------------------------


